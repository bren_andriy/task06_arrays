package com.bren.comparable_comperator;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        TwoStringContainer container = new TwoStringContainer("Ukraine","Kyiv");
        TwoStringContainer container1 = new TwoStringContainer("Germany","Berlin");
        TwoStringContainer container2 = new TwoStringContainer("England","London");
        TwoStringContainer container3 = new TwoStringContainer("France","Paris");
        TwoStringContainer container4 = new TwoStringContainer("R","R");
        TwoStringContainer container5 = new TwoStringContainer("LR","LR");
        List<TwoStringContainer> list = new ArrayList<>();
        list.add(container);
        list.add(container1);
        list.add(container2);
        list.add(container3);
        list.add(container4);
        list.add(container5);
        System.out.println("===Comparable by str1=====");

        for (TwoStringContainer element: list) {
            System.out.println(element);
        }
        System.out.println("===========");

        Collections.sort(list);
        for (TwoStringContainer element: list) {
            System.out.println(element);
        }
        System.out.println("===========");
        System.out.println("===Comparator by str2=====");
        list.sort(new ComparatorByStr2());
        for (TwoStringContainer element: list) {
            System.out.println(element);
        }
        System.out.println("===========");
        TwoStringContainer[] containers = new TwoStringContainer[3];
        containers[0] = new TwoStringContainer("Ukraine","Kyiv");
        containers[1] = new TwoStringContainer("Germany","Berlin");
        containers[2] = new TwoStringContainer("France","Paris");
        Arrays.sort(containers);
        for (TwoStringContainer element: list) {
            System.out.println(element);
        }
    }
}
