package com.bren.comparable_comperator;

public class TwoStringContainer implements Comparable<TwoStringContainer> {
    private String str1;
    private String str2;

    TwoStringContainer(String str1, String str2) {
        this.str1 = str1;
        this.str2 = str2;
    }

    String getStr1() {
        return str1;
    }

    public String getStr2() {
        return str2;
    }

    @Override
    public int compareTo(TwoStringContainer o) {
        return o.getStr1().compareTo("String");
    }

    @Override
    public String toString() {
        return "TwoStringContainer{" +
                "str1='" + str1 + '\'' +
                ", str2='" + str2 + '\'' +
                '}';
    }

}
