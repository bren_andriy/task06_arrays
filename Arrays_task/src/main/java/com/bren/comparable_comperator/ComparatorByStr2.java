package com.bren.comparable_comperator;

import java.util.Comparator;

public class ComparatorByStr2 implements Comparator<TwoStringContainer> {
    @Override
    public int compare(TwoStringContainer o1, TwoStringContainer o2) {
        return o2.getStr2().compareTo("String");
    }
}
