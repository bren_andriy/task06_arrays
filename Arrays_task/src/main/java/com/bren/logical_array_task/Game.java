package com.bren.logical_array_task;


import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;


public class Game {
    private static Scanner sc = new Scanner(System.in);
    private static Random random = new Random();
    private static int heroStrength = 25;
    private static int monsterStrength = 5 + random.nextInt(96);
    private static int artifactStrength = 10 + random.nextInt(72);
    private static String[] doors;

    public static void main(String[] args) {
        while (true) {
        printMainMenu();
        char choose = sc.next().charAt(0);
        switch (choose) {
            case 'q':
                System.out.println("Good Bye!");
                return;
            case '1':
                System.out.println(Arrays.toString(doors = fillInRoomsWithItems()));
                break;
            case '2':
                System.out.println(Arrays.toString(doors = generateRoomsWithItems()));
                break;
            default:
                System.out.println("Choose correct option!!!");
                break;
        }
            while (true) {
                printMenu();
                char choice = sc.next().charAt(0);
                switch (choice) {
                    case 'q':
                        System.out.println("Quit");
                        return;
                    case '1':
                        chooseDoor();
                        break;
                    case '2':
                        getInfoAboutRooms();
                        break;
                    case '3':
                        System.out.println(getNumberOfDoorsWhereHeroDie());
                        break;
                    case '4':
                        getSafetyWay();
                        break;
                    default:
                        System.out.println("Choose correct option!!!");
                        break;
                }
            }
        }
    }

    private static void printMenu() {
        System.out.println();
        System.out.println("===Magic Game===");
        System.out.println("1 - Select door from 1 to 10");
        System.out.println("2 - Get information about rooms");
        System.out.println("3 - Show number of doors where Hero would be died");
        System.out.println("4 - Show safety way");
        System.out.println("q - Quit");
        System.out.println("Select: ");
        System.out.println();
    }

    private static void printMainMenu() {
        System.out.println("===Magic Game===");
        System.out.println("1 - Fill in rooms with items");
        System.out.println("2 - Generate rooms with items");
    }

    private static void chooseDoor() {
        if (doors == null) {
            throw new NullPointerException("Create rooms with items");
        }
        System.out.println(Arrays.toString(doors));
        System.out.println("====Door selection====");
        System.out.println("Your select from 1 to 10 : ");
        int indexOfDoor = sc.nextInt();
        for (int i = 0; i < doors.length; i++) {
            if (i == indexOfDoor - 1) {
                if (doors[i].equals("Monster")) {
                    getFight();
                }
                if (doors[i].equals("Artifact")) {
                    getArtifact();
                }
            }
        }
    }

    private static void getFight() {
        System.out.println("Hero begin fight with Monster");
        System.out.println("Hero (" + heroStrength + "%) vs Monster (" + monsterStrength + "%)");
        if (isHeroWin()) {
            System.out.println("Hero won!!!!");
            heroStrength = 25;
        } else {
            System.out.println("Hero died!!!!");
            heroStrength = 25;
        }
    }

    private static void getArtifact() {
        System.out.println("Hero has found artifact with " + artifactStrength + "% Strength");
        heroStrength += artifactStrength;
        System.out.println("Hero Strength: " + heroStrength + "%");
    }

    private static void getInfoAboutRooms() {
        if (doors == null) {
            throw new NullPointerException("Create rooms with items");
        }
        System.out.println("===Info===");
        System.out.println("Hero strength: " + heroStrength + "% Strength");
        System.out.println("Numbers of rooms where Hero would be die: "
                + getNumberOfDoorsWhereHeroDie());
        insertItems();
    }

    private static void insertItems() {
        for (int i = 0; i < doors.length; i++) {
            System.out.print("Room number " + (i + 1) + ": ");
            if (doors[i].equals("Monster")) {
                System.out.println(" Monster with " + monsterStrength + "% Strength");
            }
            if (doors[i].equals("Artifact")) {
                System.out.println(" Artifact with " + artifactStrength + "% Strength");
            }
        }
    }

    private static boolean isHeroWin() {
        return heroStrength >= monsterStrength;
    }

    private static int getNumberOfDoorsWhereHeroDie() {
        int numberOfDoorsWhereHeroDie = 0;
        for (String element : doors) {
            if (element.equals("Monster")) {
                if (!isHeroWin()) {
                    numberOfDoorsWhereHeroDie++;
                }
            }
        }
        return numberOfDoorsWhereHeroDie;
    }

    private static String[] fillInRoomsWithItems() {
        String[] doors = new String[10];
        System.out.println("Enter 1 - Monster\nEnter 2 - Artifact");
        for (int i = 0; i < doors.length; i++) {
            System.out.println("Enter item for " + (i + 1) + " door:");
            int choice = sc.nextInt();
            fillItem(choice, i, doors);
        }
        return doors;
    }

    private static String[] generateRoomsWithItems() {
        String[] doors = new String[10];
        for (int i = 0; i < doors.length; i++) {
            int choice = 1 + random.nextInt(2);
            fillItem(choice, i, doors);
        }
        return doors;
    }

    private static void fillItem(int choice, int index, String[] doors) {
        switch (choice) {
            case 1:
                doors[index] = "Monster";
                break;
            case 2:
                doors[index] = "Artifact";
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + choice);
        }
    }

    private static void getSafetyWay() {
        int[] temp = new int[doors.length];
        int counter = 0;
        System.out.print("Safety Way: ");
        for (int i = 0; i < doors.length; i++) {
            if (doors[i].equals("Artifact")) {
                System.out.print(" " + (temp[counter++] = i + 1) + ", ");
            }
        }
        for (int i = 0; i < doors.length; i++) {
            if (doors[i].equals("Monster")) {
                System.out.print(" " + (temp[counter++] = i + 1) + ", ");
            }
        }
    }
}
