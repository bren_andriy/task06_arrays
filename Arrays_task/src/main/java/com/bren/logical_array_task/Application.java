package com.bren.logical_array_task;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class Application {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("=====Arrays====");
        createThirdArray();
    }

    private static void createThirdArray() {
        int[] array1 = {1, 1, 1, 1, 2, 3, 3, 3, 4, 4, 44, 5, 5};
        int[] array2 = {4, 5, 6, 7, 8, 9};
        int[] array3;
        int[] array5 = {5,5,5,6,6,1,1,2,5,6,3,4,4,6};
        int[] array6 = {2,1, 2, 1, 2, 1, 2, 1};
        while (true) {
            System.out.println("1.Union arrays. \n"
                    + "2.Create array like array1\n"
                    + "3.Create array like array2\n"
                    + "4.Delete duplicates which occur more than 2 time\n"
                    + "5.Delete sequence of duplicates and save one of them\n"
                    + "Q.Quit\n");
            System.out.println();
            System.out.println("Your Select: ");
            char choice = sc.nextLine().charAt(0);
            switch (choice) {
                case 'q':
                    return;
                case '1':
                    System.out.println(Arrays.toString(unionArrays(array1, array2)));
                    break;
                case '2':
                    array3 = array1;
                    System.out.println(Arrays.toString(array3));
                    System.out.println();
                    break;
                case '3':
                    array3 = array2;
                    System.out.println(Arrays.toString(array3));
                    System.out.println();
                    break;
                case '4':
                    removeTwoDuplicate(array1);
                    break;
                case '5':
                    removeDuplicate(array6);
                    break;
            }
        }
    }

    private static int[] unionArrays(int[] array1, int[] array2) {
        int maxSize = 0;
        int counter = 0;
        int[][] arrays = {array1, array2};
        for (int[] array : arrays) {
            maxSize += array.length;
        }
        int[] newArray = new int[maxSize];
        for (int[] array : arrays) {
            for (int element : array) {
                newArray[counter] = element;
                counter++;
            }
        }
        return newArray;
    }

    private static void removeTwoDuplicate(int[] array) {
        Map<Integer,Integer> map = new HashMap<>();
        for (int item : array) {
            map.put(item, map.get(item) == null ? 1 : map.get(item) + 1);
        }
        for (int value : array) {
            if (map.containsKey(value) && map.get(value) < 2) {
                System.out.print(value + " ");
            }
        }
        System.out.println();
    }

    private static void removeDuplicate(int[] array) {
        int counter = 0;
        int[] temp = new int[array.length];
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] != array[i + 1]) {
                temp[counter] = array[i];
                counter++;
            }
        }
        temp[counter] = array[array.length - 1];
        counter++;
        int[] newArray = new int[counter];
        for (int i = 0; i < counter; i++) {
            newArray[i] = temp[i];
        }
        System.out.println(Arrays.toString(newArray));
    }
}
