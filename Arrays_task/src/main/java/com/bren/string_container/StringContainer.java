package com.bren.string_container;

import java.util.Arrays;

class StringContainer {
    private static int index = 0;
    private static int sizeOfArray = 1;
    private String[] stringArray = new String[sizeOfArray];

    void addString(String string) {
        try {
            stringArray[index] = string;
        } catch (ArrayIndexOutOfBoundsException e) {
            stringArray = Arrays.copyOf(stringArray, ++sizeOfArray);
            stringArray[index] = string;
        }
        index++;
    }

    String getString(int index) {
        try {
            return stringArray[index];
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return "String with index: " + index + " doesnt exists";
    }

    String[] getStringArray() {
        return stringArray;
    }
}
