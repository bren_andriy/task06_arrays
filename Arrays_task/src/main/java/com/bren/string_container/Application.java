package com.bren.string_container;

import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        StringContainer stringContainer = new StringContainer();
        System.out.println("========");
        System.out.println("First Array");
        stringContainer.addString("Roksa");
        stringContainer.addString("Andrii");
        stringContainer.addString("Nazar");
        stringContainer.addString("Alex");
        stringContainer.addString("Dima");

        for (String element: stringContainer.getStringArray()) {
            System.out.println(element);
        }

        List<String> strings = new ArrayList<>();
        strings.add("Andri");
        strings.add("Roksa");
        strings.add("Nazar");
        strings.add("Alex");
        strings.add("Vasya");
        System.out.println();
        System.out.println("==============");
        System.out.println("Second Array: ");
        for (String element: strings) {
            System.out.println(element);
        }

    }
}
