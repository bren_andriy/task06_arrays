package com.bren.deque;

import java.lang.reflect.Array;
import java.util.Arrays;

class MyDeque {
    private int size;
    private int head;
    private int tail;
    private int[] data;

    MyDeque(int size) {
        data = new int[this.size = size];
    }

    void pushBack(int value) {
        if (++tail == size) {
            tail = 0;
        }
        data[tail] = value;
    }

    int popBack() {
        int ret = data[tail];
        if (--tail < 0) {
            tail = size - 1;
        }
        return ret;
    }

    void pushFront(int value) {
        data[head] = value;
        if (--head < 0) {
            head = size - 1;
        }
    }

    int popFront() {
        if (++head == size) {
            head = 0;
        }
        return data[head];
    }

    boolean isEmpty() {
        return head == tail;
    }
}
