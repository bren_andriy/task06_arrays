package com.bren.deque;

public class Application {
    public static void main(String[] args) {
        MyDeque deque = new MyDeque(10);
        System.out.println(deque.isEmpty());

        deque.pushBack(15);
        deque.pushBack(16);
        System.out.println(deque.popBack());
        deque.pushFront(21);
        System.out.println(deque.popFront());
    }
}
