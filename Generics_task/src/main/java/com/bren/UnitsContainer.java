package com.bren;

class UnitsContainer <T> {
    private T unit;

    void putUnit(T unit) {
        this.unit = unit;
    }

    T getUnit() {
        return this.unit;
    }


}
