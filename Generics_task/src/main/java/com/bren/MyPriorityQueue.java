package com.bren;

import java.util.*;

public class MyPriorityQueue<T> extends AbstractQueue {

    private static final int CAPACITY = 11;
    private int index = 0;
    private int size;
    private Object[] array;

    MyPriorityQueue() {
        this(CAPACITY);
    }

    private MyPriorityQueue(int capacity) {
        this.size = capacity;
        this.array = new Object[capacity];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    void insertItem(T t) {
        if (t == null) {
            throw new NullPointerException();
        }
        array[index] = t;
        index++;
    }

    private Object[] getArray() {
        return array;
    }

    void show() {
        System.out.println("===Array===");
        for (Object o: array) {
            if (o == null) {
                o = "";
            }
            System.out.print(o + " ");
        }
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    @Override
    public boolean add(Object o) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public boolean offer(Object o) {
        return false;
    }

    @Override
    public Object remove() {
        return null;
    }

    @Override
    public Object poll() {
        return null;
    }

    @Override
    public Object element() {
        return null;
    }

    @Override
    public Object peek() {
        return null;
    }
}
