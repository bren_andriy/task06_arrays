package com.bren;

public class Application {
    public static void main(String[] args) {
        UnitsContainer<Unit> container = new UnitsContainer<Unit>();
        container.putUnit(new Unit("Unit1",1));
        Unit unit = container.getUnit();
        System.out.println(unit);


        MyPriorityQueue<Integer> myPriorityQueue = new MyPriorityQueue<>();
        System.out.println(myPriorityQueue.size());
        myPriorityQueue.insertItem(1);
        myPriorityQueue.insertItem(3);
        myPriorityQueue.insertItem(4);
        myPriorityQueue.insertItem(6);
        myPriorityQueue.show();

    }
}
